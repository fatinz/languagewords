package com.example.languagewords;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by FTHY on 18.08.2014.
 */
public class Adaptor extends BaseAdapter implements GetBarViews {
     ArrayList<DilBolumleme> bolumle;
    private Context context;
    private View viewz;
    private TextView bir;
    public TextView iki;
    public TextView uc;
    public Adaptor(Context context,ArrayList<DilBolumleme> bolumle) {
        this.context=context;
        this.bolumle=bolumle;
    }
    @Override
    public int getCount() {
        return bolumle.size();
    }

    @Override
    public Object getItem(int i) {
        return bolumle.get(i);
    }

    @Override
    public long getItemId(int i) {
        return bolumle.indexOf(getItem(i));
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View cview=getbarviews();
            DilBolumleme bolumler=(DilBolumleme)getItem(i);
            bir.setText(bolumler.getDilbolgesi());
            iki.setText(bolumler.getBaslık());
            uc.setText(bolumler.getIcerik());
        return cview;
    }
    public View getbarviews(){
        View cview=viewz;
        LayoutInflater inf=(LayoutInflater)context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if(cview==null){
            cview=inf.inflate(R.layout.listayarlama,null);
            bir=(TextView)cview.findViewById(R.id.dilbolgesi);
            iki=(TextView)cview.findViewById(R.id.dilbaslıgı);
            uc=(TextView)cview.findViewById(R.id.dilicerikler);
        }
        return cview;
    }
}
