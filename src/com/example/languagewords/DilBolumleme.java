package com.example.languagewords;

/**
 * Created by FTHY on 18.08.2014.
 */
public class DilBolumleme {
    public String baslık;
    public String icerik;
    public String dilbolgesi;
    public DilBolumleme(String dilbolgesi,String baslık,String icerik){
        this.baslık=baslık;
        this.icerik=icerik;
        this.dilbolgesi=dilbolgesi;
    }
    public String getBaslık(){
        return baslık;
    }
    public void setBaslık(String baslık){
        this.baslık=baslık;
    }
    public String getIcerik(){
        return icerik;
    }
    public void setIcerik(String icerik){
        this.icerik=icerik;
    }
    public String getDilbolgesi(){
        return dilbolgesi;
    }
    public void setDilbolgesi(String dilbolgesi){
        this.dilbolgesi=dilbolgesi;
    }
}
