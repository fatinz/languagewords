package com.example.languagewords;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.view.*;
import android.widget.*;

import java.util.ArrayList;
import java.util.Locale;

public class MyActivity extends Activity implements GetBarViews {
    /**
     * Called when the activity is first created.
     */
    private ArrayList<DilBolumleme> dilb;
    private DilBolumleme diller;
    private Adaptor adaptor;
    private ListView listeler;
    private AlertDialog dialogal;
    private AlertDialog.Builder dialogayarlama;
    private View views;
    private TextView dilbs;
    private EditText diltextbolgesi;
    private TextView diltit;
    private EditText dilbaslıgı;
    private TextView dilicerik;
    private EditText dilicerikleri;
    private TextToSpeech talk;
    private int results;
    private  String dilicerikler;
    private String dilbaslık;
    private String dilal;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        listeler=(ListView)findViewById(R.id.liste);
        dilb=new ArrayList<DilBolumleme>();
        adaptor=new Adaptor(this,dilb);
        listeler.setOnItemClickListener(ıtemlistener);
        talk=new TextToSpeech(MyActivity.this,listeners);
        listeler.setAdapter(adaptor);


    }
    private TextToSpeech.OnInitListener listeners=new TextToSpeech.OnInitListener() {
        @Override
        public void onInit(int statu) {
            if(statu == TextToSpeech.SUCCESS){
                 DilAyarla(Locale.US);
            }else{
                 toastayarla();
            }
        }
    };
    private AdapterView.OnItemClickListener ıtemlistener=new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                  DilBolumleme blm=(DilBolumleme)dilb.get(i);
                  if(results == TextToSpeech.LANG_NOT_SUPPORTED || results == TextToSpeech.LANG_MISSING_DATA){
                      Toast.makeText(getApplicationContext(),"NOT SUPPORTED LANGUAGE",Toast.LENGTH_LONG).show();
                  }else{
                      talk.speak(blm.getIcerik(),TextToSpeech.QUEUE_FLUSH,null);
                  }

        }
    };
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inf=getMenuInflater();
        inf.inflate(R.menu.menus,menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.bir:
                dialogayarlama=new AlertDialog.Builder(this);
                dialogayarlama.setView(getbarviews());
                dialogayarlama.setTitle("LANGUAGE ACTİONS");
                dialogayarlama.setCancelable(true);
                dialogayarlama.setPositiveButton("CANCEL",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                           dialogal.cancel();
                    }
                })
                .setNegativeButton("SHARE",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                         dilal=diltextbolgesi.getText().toString();
                         dilbaslık=dilbaslıgı.getText().toString();
                         dilicerikler=dilicerikleri.getText().toString();
                         dilb.add(diller=new DilBolumleme(dilal,dilbaslık,dilicerikler));
                         adaptor.notifyDataSetChanged();

                    }
                });
                dialogal=dialogayarlama.create();
                dialogal.show();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    public View getbarviews(){
        View view=views;
        LayoutInflater inf=(LayoutInflater)getApplicationContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if(view==null){
            view=inf.inflate(R.layout.bardialogu,null);
            dilbs=(TextView)view.findViewById(R.id.lanzone);
            diltextbolgesi=(EditText)view.findViewById(R.id.languagezones);
            diltit=(TextView)view.findViewById(R.id.lantit);
            dilbaslıgı=(EditText)view.findViewById(R.id.lantitles);
            dilicerik=(TextView)view.findViewById(R.id.lancontents);
            dilicerikleri=(EditText)view.findViewById(R.id.dilıcerikleri);
        }
        return view;
    }
    public void DilAyarla(Locale loc){
        results=talk.setLanguage(Locale.US);
    }
    public void toastayarla(){
         Toast.makeText(getApplicationContext(),"NOT SUPPORTED YOUR LANGUAGE",Toast.LENGTH_LONG).show();
    }
}
